# PCK
Network port checker.

![pck-menu](https://user-images.githubusercontent.com/33821311/33052887-4d74552c-ceb4-11e7-9ce3-d20d7ae71b23.png)

## Note on Linux Support:

This program uses "github.com/sparrc/go-ping" library. The library attempts to send an
"unprivileged" ping via UDP. On linux, this must be enabled by setting

```
sudo sysctl -w net.ipv4.ping_group_range="0   2147483647"
```

If you do not wish to do this, you can set `pinger.SetPrivileged(true)` and
use setcap to allow your binary using go-ping to bind to raw sockets
(or just run as super-user):

```
setcap cap_net_raw=+ep /bin/goping-binary
```

See [this blog](https://sturmflut.github.io/linux/ubuntu/2015/01/17/unprivileged-icmp-sockets-on-linux/)
and [the Go icmp library](https://godoc.org/golang.org/x/net/icmp) for more details.
