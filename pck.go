// The pck utility performs three network tests (i.e., Ping test, TCP port test, UDP port test).
// The purpose of this program is provide non-technical end-users a simple tool for verifying
// network host and port access.

package main

import (
	"fmt"
	"github.com/sparrc/go-ping"
	"net"
	"os"
	"os/exec"
	"strconv"
	"time"
)

const (
	minTCPPort = 1
	maxTCPPort = 65535
)

var ip, port string

// Get IP address
func GetIpaddr() {
	fmt.Print("Enter IP address to check: ")
	_, err := fmt.Scan(&ip)
	if err != nil {
		panic(err)
	}
	addr := net.ParseIP(ip)
	fmt.Println()
	if addr == nil {
		fmt.Printf("Invalid IP address.\n")
		os.Exit(1)
	}
}

// Get Port number
func GetPortnumber() {
	fmt.Print("Enter port to check: ")
	_, err := fmt.Scan(&port)
	if err != nil {
		panic(err)
	}
	v, _ := strconv.Atoi(port)
	if v < minTCPPort || v > maxTCPPort {
		fmt.Println("Invalid port number")
		fmt.Println()
		os.Exit(1)
	}
}

// Ping test function
func PingTest() {
	GetIpaddr()
	fmt.Println("Press 'Control-C' to stop ping test.")
	// Pinger reference. https://github.com/sparrc/go-ping
	// Feature request: Add handling for Destination Host Unreachable
	pinger, err := ping.NewPinger(ip)
	if err != nil {
		panic(err)
	}
	pinger.OnRecv = func(pkt *ping.Packet) {
		fmt.Printf("%d bytes from %s: icmp_seq=%d time=%v\n",
			pkt.Nbytes, pkt.IPAddr, pkt.Seq, pkt.Rtt)
	}
	pinger.OnFinish = func(stats *ping.Statistics) {
		fmt.Printf("\n--- %s ping statistics ---\n", stats.Addr)
		fmt.Printf("%d packets transmitted, %d packets received, %v%% packet loss\n",
			stats.PacketsSent, stats.PacketsRecv, stats.PacketLoss)
		fmt.Printf("round-trip min/avg/max/stddev = %v/%v/%v/%v\n",
			stats.MinRtt, stats.AvgRtt, stats.MaxRtt, stats.StdDevRtt)
	}
	fmt.Printf("PING %s (%s):\n", pinger.Addr(), pinger.IPAddr())
	pinger.Run()
}

// TCP test function
func TcpTest() {
	GetIpaddr()
	GetPortnumber()
	hostIP := fmt.Sprintf("%s: %s", ip, port)
	fmt.Println()
	_, err := net.DialTimeout("tcp", hostIP, 10*time.Millisecond)
	if err != nil {
		fmt.Printf("Port %s on %s is closed.", port, ip)
	} else {
		fmt.Printf("Port %s on %s is open.", port, ip)
	}

}

// UDP test function. This feature is coming soon.
// Get IPaddr:port, setup listener, connect, send packet, determine open/close status
//func UdpTest() {
//	GetIpaddr()
//	GetPortnumber()
//	hostIP := fmt.Sprintf("%s: %s", ip, port)
//
//}

// Clear screen functionq
func ClearScreen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

// Displaying a menu function
func DisplayMenu() {
	ClearScreen()
	fmt.Println()
	fmt.Println("*****************************")
	fmt.Println(" Port Checker. Version 0.1.4 ")
	fmt.Println("*****************************")
	fmt.Println()
	fmt.Println(" p  Ping (icmp) Test")
	fmt.Println(" t  TCP Port Test")
	fmt.Println(" u  UDP Port Test")
	fmt.Println()
	fmt.Println(" c  Clear screen")
	fmt.Println(" q  Quit")
	fmt.Println()
	fmt.Println("Type a letter and press Enter to make a selection.")
	fmt.Println()
}

// Decision function
func MakeDecision() {
	var s string
	for {
		fmt.Scanf("%s", &s)
		fmt.Println()
		switch s {
		case "t": // TCP test selection
			DisplayMenu()
			TcpTest()
			fmt.Println()
			fmt.Println("Make another selection, or press q to quit.")
			fmt.Println()
		case "u": // UDP test selection
			DisplayMenu()
//			UdpTest()
			fmt.Print("UDP Port Test function is not available. Coming soon.")
			fmt.Println()
			fmt.Println("Make another selection, or press q to quit.")
			fmt.Println()
		case "p": // Ping test selection
			DisplayMenu()
			PingTest()
			fmt.Println()
			fmt.Println("Press m to display menu, or press q to quit.")
			fmt.Println()
		case "c": // Clear the screen
			DisplayMenu()
		case "m": // Show Menu
			DisplayMenu()
		case "q": // Quit
			os.Exit(0)
		default:
			ClearScreen()
			DisplayMenu()
			fmt.Println("Press a valid key.")
		}
	}
}

// Main function
func main() {
	DisplayMenu()
	MakeDecision()
}
